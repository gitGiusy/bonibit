﻿Module Ambiente
 

    ''----------Parametri passati da AC --------------------------------------
    Public NomeServer As String '"bobi"
    Public NomeDBGis As String '"CataAmmi2"
    Public IDUtente As Integer   '101 priv 2 '102 priv 1 , 1 superUser


    Public Sub Avvia()


        Try
            Dim fr As New frAvvioStandalone
            fr.MostraDB = True
            fr.ShowDialog()
            If fr.DialogResult = Windows.Forms.DialogResult.OK Then
                NomeServer = fr.NomeServer
                NomeDBGis = fr.NomeDB
                IDUtente = fr.Utente
            Else
                End
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try







    End Sub


   

#Region "Connessioni"

 

    Public Function NuovaConnessioneDB() As SqlClient.SqlConnection
        Try
            Dim c As String = DBbase.StringaConnessione(NomeDBGis, NomeServer)
            Dim con As New SqlClient.SqlConnection(c)
            'con.Open()
            Return con
        Catch ex As Exception
            MsgBox("Errore nel tentativo di stabilire una connessione al DB: " & NomeServer & "." & NomeDBGis & vbCrLf & ex.Message)
            Return Nothing
        End Try
    End Function
    Public Function NuovaConnessioneTempDB(Server As String) As SqlClient.SqlConnection
        Try
            Dim c As String = DBbase.StringaConnessione("TempDB", Server)
            Dim con As New SqlClient.SqlConnection(c)
            con.Open()
            Return con
        Catch ex As Exception
            MsgBox("Errore nel tentativo di stabilire una connessione al DB: " & Server & ".TempDB" & vbCrLf & ex.Message)
            Return Nothing
        End Try
    End Function
#End Region


    ''' 
    Public Class DBbase

        Dim mconnessione As New SqlClient.SqlConnection
        Public NomeServer As String
        Public NomeDB As String

        Public Sub New(ByVal Server As String, ByVal DB As String)
            Try
                ' MsgBox("aaxx DBase.new1")
                Me.NomeServer = Server
                Me.NomeDB = DB
                Me.ChiudiConnessione()
                ' If Not IsNothing(mconnessione) Then If Not mconnessione.State = ConnectionState.Closed Then mconnessione.Close()
                mconnessione.ConnectionString = StringaConnessione(DB, Server)
                Me.ApriConnessione()
                'MsgBox("aaxx DBase.new2")
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub New(ByVal Connessione As SqlClient.SqlConnection)
            Try
                mconnessione = Connessione
                Me.ApriConnessione()
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub ChiudiConnessione()
            Try
                If Not IsNothing(mconnessione) Then If Not mconnessione.State = ConnectionState.Closed Then mconnessione.Close()
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub ApriConnessione()
            Try
                If Not IsNothing(mconnessione) Then If Not mconnessione.State = ConnectionState.Open Then mconnessione.Open()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public ReadOnly Property Connessione() As Data.SqlClient.SqlConnection
            Get
                Return mconnessione
            End Get
        End Property



        Public Shared Function StringaConnessione(ByVal DB As String, ByVal Server As String) As String
            Try
                Dim cs As String
                cs = "Data Source= " & Server
                cs = cs & "; initial Catalog=" & DB
                cs = cs & "; pooling=false"
                cs = cs & "; User ID=Cata"
                cs = cs & ";Password=catasagile"
                Return cs
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public ReadOnly Property StatoConnessione() As System.Data.ConnectionState
            Get
                Return Me.Connessione.State
            End Get
        End Property


    End Class
End Module