﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frAvvioStandalone
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PanelDB = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lbElencoDBServer = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DB = New System.Windows.Forms.ComboBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Server = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxtUtente = New System.Windows.Forms.TextBox()
        Me.Annulla = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.MaskedTextBox3 = New System.Windows.Forms.MaskedTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanelDB.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelDB
        '
        Me.PanelDB.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PanelDB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanelDB.Controls.Add(Me.Button1)
        Me.PanelDB.Controls.Add(Me.lbElencoDBServer)
        Me.PanelDB.Controls.Add(Me.Label2)
        Me.PanelDB.Controls.Add(Me.Label5)
        Me.PanelDB.Controls.Add(Me.DB)
        Me.PanelDB.Location = New System.Drawing.Point(14, 87)
        Me.PanelDB.Name = "PanelDB"
        Me.PanelDB.Size = New System.Drawing.Size(225, 286)
        Me.PanelDB.TabIndex = 26
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(162, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(46, 23)
        Me.Button1.TabIndex = 17
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lbElencoDBServer
        '
        Me.lbElencoDBServer.FormattingEnabled = True
        Me.lbElencoDBServer.Location = New System.Drawing.Point(9, 56)
        Me.lbElencoDBServer.Name = "lbElencoDBServer"
        Me.lbElencoDBServer.Size = New System.Drawing.Size(199, 212)
        Me.lbElencoDBServer.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(22, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "DB"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 38)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "ElencoDBServer"
        '
        'DB
        '
        Me.DB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.DB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.DB.FormattingEnabled = True
        Me.DB.Location = New System.Drawing.Point(34, 8)
        Me.DB.Name = "DB"
        Me.DB.Size = New System.Drawing.Size(174, 21)
        Me.DB.TabIndex = 13
        '
        'Server
        '
        Me.Server.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.Server.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.Server.FormattingEnabled = True
        Me.Server.Location = New System.Drawing.Point(55, 27)
        Me.Server.Name = "Server"
        Me.Server.Size = New System.Drawing.Size(184, 21)
        Me.Server.TabIndex = 25
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(255, 131)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Password"
        Me.Label4.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(103, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "idUtente"
        Me.Label3.Visible = False
        '
        'TxtUtente
        '
        Me.TxtUtente.Location = New System.Drawing.Point(156, 54)
        Me.TxtUtente.Name = "TxtUtente"
        Me.TxtUtente.Size = New System.Drawing.Size(83, 20)
        Me.TxtUtente.TabIndex = 22
        Me.TxtUtente.Visible = False
        '
        'Annulla
        '
        Me.Annulla.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Annulla.Location = New System.Drawing.Point(278, 57)
        Me.Annulla.Name = "Annulla"
        Me.Annulla.Size = New System.Drawing.Size(75, 23)
        Me.Annulla.TabIndex = 21
        Me.Annulla.Text = "Annulla"
        Me.Annulla.UseVisualStyleBackColor = True
        '
        'OK
        '
        Me.OK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.OK.Location = New System.Drawing.Point(278, 20)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(75, 23)
        Me.OK.TabIndex = 20
        Me.OK.Text = "OK"
        Me.OK.UseVisualStyleBackColor = True
        '
        'MaskedTextBox3
        '
        Me.MaskedTextBox3.Location = New System.Drawing.Point(314, 128)
        Me.MaskedTextBox3.Name = "MaskedTextBox3"
        Me.MaskedTextBox3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.MaskedTextBox3.Size = New System.Drawing.Size(39, 20)
        Me.MaskedTextBox3.TabIndex = 19
        Me.MaskedTextBox3.Text = "catasagile"
        Me.MaskedTextBox3.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Server"
        '
        'frAvvioStandalone
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(371, 417)
        Me.Controls.Add(Me.PanelDB)
        Me.Controls.Add(Me.Server)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TxtUtente)
        Me.Controls.Add(Me.Annulla)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.MaskedTextBox3)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frAvvioStandalone"
        Me.Text = "frAvvioStandalone"
        Me.PanelDB.ResumeLayout(False)
        Me.PanelDB.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelDB As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lbElencoDBServer As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DB As System.Windows.Forms.ComboBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Server As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TxtUtente As System.Windows.Forms.TextBox
    Friend WithEvents Annulla As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents MaskedTextBox3 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
