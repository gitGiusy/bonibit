﻿Public Class frAvvioStandalone

    Public MostraDB As Boolean = True

    Public ReadOnly Property NomeServer() As String
        Get
            Return Server.Text
        End Get
    End Property
    Public ReadOnly Property NomeDB() As String
        Get
            Return DB.Text
        End Get
    End Property
    Public ReadOnly Property Utente() As Integer
        Get
            Return Val(TxtUtente.Text)
        End Get
    End Property
    Private Sub MaskedTextBox3_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MaskedTextBox3.Validated
        If MaskedTextBox3.Text = "catasagile" Then Me.OK.Enabled = True
    End Sub

    Private Sub MaskedTextBox3_MaskInputRejected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MaskInputRejectedEventArgs) Handles MaskedTextBox3.MaskInputRejected

    End Sub

    Private Sub frAvvioStandalone_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If MaskedTextBox3.Text = "catasagile" Then Me.OK.Enabled = True Else Me.OK.Enabled = False
        Me.OK.Select()
        Me.PanelDB.Visible = MostraDB
        Try
            Dim fS As String = "Standalone.txt"
            If FileIO.FileSystem.FileExists(fS) Then
                'FileOpen(1, fS, OpenMode.Input)
                Dim sr As New IO.StreamReader(fS)
                While Not sr.EndOfStream
                    Dim r As String = sr.ReadLine
                    Dim a(1) As String
                    a = r.Split(",")
                    If a(1).Length > 0 Then If a(0).ToLower = "server" Then Me.Server.Items.Add(a(1))
                    If a(1).Length > 0 Then If a(0).ToLower = "db" Then Me.DB.Items.Add(a(1))
                    If a(0).ToLower = "serverc" Then Me.Server.Text = a(1)
                    If a(0).ToLower = "dbc" Then Me.DB.Text = a(1)
                End While
                sr.Close() : sr = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Try
            Dim fS As String = "Standalone.txt"
            Dim sW As New IO.StreamWriter(fS, False)
            For Each s As String In Me.Server.Items
                If s.Length > 0 Then sW.Write("Server," & s & vbCrLf)
            Next
            For Each s As String In Me.DB.Items
                If s.Length > 0 Then sW.Write("DB," & s & vbCrLf)
            Next
            sW.Write("ServerC," & Me.Server.Text & vbCrLf)
            sW.Write("DBC," & Me.DB.Text & vbCrLf)
            sW.Close() : sW = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Server_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Server.Validated
        Try
            If Not Me.Server.Items.Contains(Me.Server.Text) Then Me.Server.Items.Add(Me.Server.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DB_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DB.Validated
        Try
            If Not Me.DB.Items.Contains(Me.DB.Text) Then Me.DB.Items.Add(Me.DB.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Non completata, carica il DB del Server
    Private Sub MostraDBS()
        Try
            If Me.Server.SelectedItem Is Nothing Then Exit Sub

            Dim sql As String = "SELECT name AS Nome FROM sys.databases " &
                                    " WHERE (name <> 'master') AND (name <> 'tempDB') AND (name <> 'model') AND (name <> 'msdb') AND (NOT (name LIKE N'ReportServer%')) AND (NOT (name LIKE N'ReportServerTempDB%'))" &
                                    " ORDER BY Nome"
            Dim ta As New SqlClient.SqlDataAdapter(sql, NuovaConnessioneTempDB(Me.Server.SelectedItem))
            Dim t As New Data.DataTable("ElencoDB")
            ta.Fill(t)
            '  Me.lbElencoDBServer.DataSource = Nothing
            Me.lbElencoDBServer.DataSource = t
            Me.lbElencoDBServer.DisplayMember = "Nome"
            '  Me.taListaDBs.Fill(Me.DsConfrontaDBs.ListaDB)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub Server_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles Server.SelectedIndexChanged
        MostraDBS()
    End Sub

    Private Sub lbElencoDBServer_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lbElencoDBServer.DoubleClick
        AggiungiDBAllelenco()
    End Sub

    Private Sub AggiungiDBAllelenco()
        Try
            Dim s As String = Me.lbElencoDBServer.Text
            If s.Length = 0 Then Exit Sub
            Dim trovato As Boolean = False
            Dim i As Integer = Me.DB.FindString(s)
            If i = -1 Then i = Me.DB.Items.Add(s)
            Me.DB.SelectedIndex = i
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        AggiungiDBAllelenco()
    End Sub

End Class