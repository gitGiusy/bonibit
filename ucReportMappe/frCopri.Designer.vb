﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frCopri
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frCopri))
        Me.tbZoom = New System.Windows.Forms.TrackBar()
        Me.brZoomIndietro = New System.Windows.Forms.Button()
        Me.brZoomAvanti = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.btPanOvest = New System.Windows.Forms.Button()
        Me.btPanNord = New System.Windows.Forms.Button()
        Me.btPanEst = New System.Windows.Forms.Button()
        Me.btPanSud = New System.Windows.Forms.Button()
        Me.btPanNo = New System.Windows.Forms.Button()
        Me.brZoomAll = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.tbZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbZoom
        '
        Me.tbZoom.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel1.SetColumnSpan(Me.tbZoom, 3)
        Me.tbZoom.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.tbZoom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbZoom.LargeChange = 1
        Me.tbZoom.Location = New System.Drawing.Point(16, 150)
        Me.tbZoom.Margin = New System.Windows.Forms.Padding(16, 0, 0, 0)
        Me.tbZoom.Name = "tbZoom"
        Me.tbZoom.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbZoom.Size = New System.Drawing.Size(59, 96)
        Me.tbZoom.TabIndex = 0
        Me.tbZoom.TabStop = False
        Me.tbZoom.TickStyle = System.Windows.Forms.TickStyle.Both
        '
        'brZoomIndietro
        '
        Me.brZoomIndietro.BackgroundImage = CType(resources.GetObject("brZoomIndietro.BackgroundImage"), System.Drawing.Image)
        Me.brZoomIndietro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.brZoomIndietro.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.brZoomIndietro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.brZoomIndietro.Location = New System.Drawing.Point(25, 246)
        Me.brZoomIndietro.Margin = New System.Windows.Forms.Padding(0)
        Me.brZoomIndietro.Name = "brZoomIndietro"
        Me.brZoomIndietro.Size = New System.Drawing.Size(25, 25)
        Me.brZoomIndietro.TabIndex = 1
        Me.brZoomIndietro.UseVisualStyleBackColor = True
        '
        'brZoomAvanti
        '
        Me.brZoomAvanti.BackgroundImage = CType(resources.GetObject("brZoomAvanti.BackgroundImage"), System.Drawing.Image)
        Me.brZoomAvanti.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.brZoomAvanti.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.brZoomAvanti.Dock = System.Windows.Forms.DockStyle.Fill
        Me.brZoomAvanti.Location = New System.Drawing.Point(25, 125)
        Me.brZoomAvanti.Margin = New System.Windows.Forms.Padding(0)
        Me.brZoomAvanti.Name = "brZoomAvanti"
        Me.brZoomAvanti.Size = New System.Drawing.Size(25, 25)
        Me.brZoomAvanti.TabIndex = 2
        Me.brZoomAvanti.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.brZoomAvanti, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.brZoomIndietro, 1, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.tbZoom, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.btPanOvest, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btPanNord, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btPanEst, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btPanSud, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.btPanNo, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.brZoomAll, 1, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 12)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(75, 271)
        Me.TableLayoutPanel1.TabIndex = 3
        '
        'btPanOvest
        '
        Me.btPanOvest.BackgroundImage = CType(resources.GetObject("btPanOvest.BackgroundImage"), System.Drawing.Image)
        Me.btPanOvest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btPanOvest.Cursor = System.Windows.Forms.Cursors.PanWest
        Me.btPanOvest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btPanOvest.Location = New System.Drawing.Point(0, 25)
        Me.btPanOvest.Margin = New System.Windows.Forms.Padding(0)
        Me.btPanOvest.Name = "btPanOvest"
        Me.btPanOvest.Size = New System.Drawing.Size(25, 25)
        Me.btPanOvest.TabIndex = 4
        Me.btPanOvest.UseVisualStyleBackColor = True
        '
        'btPanNord
        '
        Me.btPanNord.BackgroundImage = CType(resources.GetObject("btPanNord.BackgroundImage"), System.Drawing.Image)
        Me.btPanNord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btPanNord.Cursor = System.Windows.Forms.Cursors.PanNorth
        Me.btPanNord.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btPanNord.Location = New System.Drawing.Point(25, 0)
        Me.btPanNord.Margin = New System.Windows.Forms.Padding(0)
        Me.btPanNord.Name = "btPanNord"
        Me.btPanNord.Size = New System.Drawing.Size(25, 25)
        Me.btPanNord.TabIndex = 5
        Me.btPanNord.UseVisualStyleBackColor = True
        '
        'btPanEst
        '
        Me.btPanEst.BackgroundImage = CType(resources.GetObject("btPanEst.BackgroundImage"), System.Drawing.Image)
        Me.btPanEst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btPanEst.Cursor = System.Windows.Forms.Cursors.PanEast
        Me.btPanEst.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btPanEst.Location = New System.Drawing.Point(50, 25)
        Me.btPanEst.Margin = New System.Windows.Forms.Padding(0)
        Me.btPanEst.Name = "btPanEst"
        Me.btPanEst.Size = New System.Drawing.Size(25, 25)
        Me.btPanEst.TabIndex = 6
        Me.btPanEst.UseVisualStyleBackColor = True
        '
        'btPanSud
        '
        Me.btPanSud.BackgroundImage = CType(resources.GetObject("btPanSud.BackgroundImage"), System.Drawing.Image)
        Me.btPanSud.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btPanSud.Cursor = System.Windows.Forms.Cursors.PanSouth
        Me.btPanSud.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btPanSud.Location = New System.Drawing.Point(25, 50)
        Me.btPanSud.Margin = New System.Windows.Forms.Padding(0)
        Me.btPanSud.Name = "btPanSud"
        Me.btPanSud.Size = New System.Drawing.Size(25, 25)
        Me.btPanSud.TabIndex = 7
        Me.btPanSud.UseVisualStyleBackColor = True
        '
        'btPanNo
        '
        Me.btPanNo.Cursor = System.Windows.Forms.Cursors.NoMove2D
        Me.btPanNo.Location = New System.Drawing.Point(28, 28)
        Me.btPanNo.Name = "btPanNo"
        Me.btPanNo.Size = New System.Drawing.Size(19, 19)
        Me.btPanNo.TabIndex = 8
        Me.btPanNo.UseVisualStyleBackColor = True
        '
        'brZoomAll
        '
        Me.brZoomAll.BackgroundImage = CType(resources.GetObject("brZoomAll.BackgroundImage"), System.Drawing.Image)
        Me.brZoomAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.brZoomAll.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.brZoomAll.Dock = System.Windows.Forms.DockStyle.Fill
        Me.brZoomAll.Location = New System.Drawing.Point(25, 100)
        Me.brZoomAll.Margin = New System.Windows.Forms.Padding(0)
        Me.brZoomAll.Name = "brZoomAll"
        Me.brZoomAll.Size = New System.Drawing.Size(25, 25)
        Me.brZoomAll.TabIndex = 3
        Me.brZoomAll.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(519, 467)
        Me.Panel1.TabIndex = 4
        '
        'frCopri
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(519, 467)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frCopri"
        Me.Opacity = 0.2R
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form1"
        Me.TransparencyKey = System.Drawing.Color.Gainsboro
        CType(Me.tbZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbZoom As System.Windows.Forms.TrackBar
    Friend WithEvents brZoomIndietro As System.Windows.Forms.Button
    Friend WithEvents brZoomAvanti As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents brZoomAll As System.Windows.Forms.Button
    Friend WithEvents btPanOvest As System.Windows.Forms.Button
    Friend WithEvents btPanNord As System.Windows.Forms.Button
    Friend WithEvents btPanEst As System.Windows.Forms.Button
    Friend WithEvents btPanSud As System.Windows.Forms.Button
    Friend WithEvents btPanNo As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
