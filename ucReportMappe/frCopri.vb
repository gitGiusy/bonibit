﻿Public Class frCopri

 
    Public Event ClickToPoint(P0 As Point)
    Public Event ClickToPointAndZoom(P0 As Point, Zoom As Integer)
    Public Event Zoom(Valore As Integer)
    Public Event Pan(DX As Integer, DY As Integer)
    Public Event ZoomAll()

    Dim curZoom As Cursor
    Sub New(_Stato As ucReportMappe.eStatoVisualizzatore)
        InitializeComponent()
        Stato = _Stato
    End Sub
    Dim mStato As ucReportMappe.eStatoVisualizzatore
    Public Property Stato As ucReportMappe.eStatoVisualizzatore
        Set(value As ucReportMappe.eStatoVisualizzatore)

            Try
                mStato = value
                Select Case mStato
                    Case ucReportMappe.eStatoVisualizzatore.Pan
                        Me.Cursor = Cursors.Hand
                        Me.Panel1.Cursor = Cursors.Hand
                    Case ucReportMappe.eStatoVisualizzatore.ZommSelezione
                        Me.Cursor = Cursors.Cross
                        Me.Panel1.Cursor = Cursors.Cross
                    Case ucReportMappe.eStatoVisualizzatore.ZoomAvanti
                        Me.Cursor = New Cursor(Me.GetType(), "ZoomAvanti.cur")
                        Me.Panel1.Cursor = Me.Cursor
                    Case ucReportMappe.eStatoVisualizzatore.ZoomIndietro
                        Me.Cursor = New Cursor(Me.GetType(), "ZoomIndietro.cur")
                        Me.Panel1.Cursor = Me.Cursor
          
                End Select
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End Set
        Get
            Return mStato
        End Get
    End Property


#Region "Eventi Mouse"
    Dim isDown As Boolean = False
    Dim initialX As Integer
    Dim initialY As Integer
    ' 
    Private Sub Form1_MouseDown(sender As Object, e As MouseEventArgs) Handles Panel1.MouseDown
        If mStato <> ucReportMappe.eStatoVisualizzatore.ZommSelezione Then Exit Sub
        isDown = True
        initialX = e.X
        initialY = e.Y
    End Sub

    Private Sub Form1_MouseMove(sender As Object, e As MouseEventArgs) Handles Panel1.MouseMove
        If isDown = False Or mStato <> ucReportMappe.eStatoVisualizzatore.ZommSelezione Then Exit Sub
        Try
            Dim formGraphics As System.Drawing.Graphics
            Me.Refresh() 'elimima rettagolo
            Dim drwaPen As Pen = New Pen(Color.Navy, 1)
            Dim rect As Rectangle = New Rectangle(Math.Min(e.X, initialX),
                               Math.Min(e.Y, initialY),
                               Math.Abs(e.X - initialX),
                               Math.Abs(e.Y - initialY))
            formGraphics = Panel1.CreateGraphics()
            formGraphics.DrawRectangle(drwaPen, rect)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Form1_Mouseup(sender As Object, e As MouseEventArgs) Handles Panel1.MouseUp
        Try
            If isDown = True Then Me.Refresh() 'elimima rettagolo
            isDown = False
            Select Case mStato
                Case ucReportMappe.eStatoVisualizzatore.Pan, _
                    ucReportMappe.eStatoVisualizzatore.ZoomAvanti, _
                    ucReportMappe.eStatoVisualizzatore.ZoomIndietro
                    RaiseEvent ClickToPoint(e.Location)
                Case ucReportMappe.eStatoVisualizzatore.ZommSelezione
                    If initialX - e.X = 0 Or initialY - e.Y = 0 Then Exit Sub
                    Dim p0 As New Point((initialX + e.X) / 2, (initialY + e.Y) / 2)
                    Dim zx As Single = Me.Panel1.Width / Math.Abs(initialX - e.X)
                    Dim zy As Single = Me.Panel1.Height / Math.Abs(initialY - e.Y)
                    If Math.Min(zx, zy) <= 0.1 Then Exit Sub
                    Dim z As Single = Int(Math.Log(Math.Min(zx, zy), 2))
                    RaiseEvent ClickToPointAndZoom(p0, z)
            End Select
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub
#End Region

#Region "Comandi Pannello"

    Private Sub btPanNord_Click(sender As Object, e As EventArgs) Handles btPanNord.Click
        RaiseEvent Pan(0, 1)
    End Sub

    Private Sub btPanEst_Click(sender As Object, e As EventArgs) Handles btPanEst.Click
        RaiseEvent Pan(1, 0)
    End Sub

    Private Sub btPanOvest_Click(sender As Object, e As EventArgs) Handles btPanOvest.Click
        RaiseEvent Pan(-1, 0)
    End Sub

    Private Sub btPanSud_Click(sender As Object, e As EventArgs) Handles btPanSud.Click
        RaiseEvent Pan(0, -1)
    End Sub

    Private Sub btPanNo_Click(sender As Object, e As EventArgs) Handles btPanNo.Click
        RaiseEvent Pan(0, 0)
    End Sub

    Private Sub brZoomAll_Click(sender As Object, e As EventArgs) Handles brZoomAll.Click
        RaiseEvent ZoomAll()
    End Sub

    Private Sub brZoomAvanti_Click(sender As Object, e As EventArgs) Handles brZoomAvanti.Click
        If Me.tbZoom.Maximum > Me.tbZoom.Value Then RaiseEvent Zoom(Me.tbZoom.Value + 1)
    End Sub

    Private Sub brZoomIndietro_Click(sender As Object, e As EventArgs) Handles brZoomIndietro.Click
        If Me.tbZoom.Minimum < Me.tbZoom.Value Then RaiseEvent Zoom(Me.tbZoom.Value - 1)
    End Sub
    Private Sub tbZoom_MouseUp(sender As Object, e As MouseEventArgs) Handles tbZoom.MouseUp
        RaiseEvent Zoom(tbZoom.Value)
    End Sub

#End Region

#Region "Tentativi Pan Abbandonati Pan con trascinamento"
    'Dim panAvviato As Boolean = False
    'Dim panPointStart As Point = Nothing
    'Private Sub frCopri_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
    '    If mStato <> ucReportMappe.eStatoVisualizzatore.ZommSelezione Then Exit Sub
    '    Dim cursor As Cursor = Me.Cursor
    '  Dim graphics As Graphics = Me.CreateGraphics()
    '    Dim rectangle As New Rectangle(New Point(10, 10), _
    '      New Size(Cursor.Size.Width * 2, Cursor.Size.Height * 2))
    '    Cursor.DrawStretched(graphics, rectangle)
    '    Exit Sub
    '    ' Draw the cursor in normal size.
    '    rectangle.Location = New Point(rectangle.Width + _
    '      rectangle.Location.X, rectangle.Height + rectangle.Location.Y)
    '    rectangle.Size = Cursor.Size
    '    Cursor.Draw(graphics, rectangle)

    '    ' Dispose of the cursor.
    '    Cursor.Dispose()
    '    Exit Sub
    '    Try
    '        panPointStart = e.Location
    '        Dim p0 As New Point(Me.Width / 2, Me.Height / 2)
    '        Dim bm As New Bitmap(Me.Width, Me.Height)

    '        'Dim cr As New Cursor
    '        Dim g As Graphics = Graphics.FromImage(bm)
    '        'g.RotateTransform(30.0F)
    '        'g.TranslateTransform(300.0F, 300.0F)
    '        g.CopyFromScreen(Me.PointToScreen(New Point(0, 0)), New Point(0, 0), Me.Size)

    '        '   g.TranslateTransform(panPointStart.X - p0.X, panPointStart.Y - p0.Y)
    '        ' Dim bm As New Bitmap(PictureBox1.Image, Me.PictureBox1.Size)
    '        ' Me.Cursor = New Cursor(bm.GetHicon)
    '        Me.Cursor.Draw(g, New Rectangle(e.Location, New Size(100, 100)))
    '        panAvviato = True
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub
    'Private Sub frCopri_MouseUp(sender As Object, e As MouseEventArgs) Handles MyBase.MouseUp
    '    Me.Cursor = Cursors.Default
    '    panAvviato = False
    '    panPointStart = Nothing
    'End Sub

    'Private Sub frCopri_MouseMove(sender As Object, e As MouseEventArgs) ' Handles MyBase.MouseMove
    '    If mStato <> ucReportMappe.eStatoVisualizzatore.ZommSelezione Or panAvviato = False Then Exit Sub
    '    Try

    '        Dim dx As Integer = e.X - panPointStart.X
    '        Dim dy As Integer = -e.Y + panPointStart.Y
    '        Dim bm As New Bitmap(Me.Width - Math.Abs(dx), Me.Height - Math.Abs(dy))
    '        Dim x0 As Integer = IIf(dx > 0, 0, dx)
    '        Dim y0 As Integer = IIf(dy > 0, 0, dy)
    '        Dim p0 As New Point(x0, y0)
    '        'Dim x0 As Integer = Me.Width / 2 ' Me.Location.X + 
    '        'Dim sx As Integer = e.X - x0

    '        'Dim cr As New Cursor
    '        Dim g As Graphics = Graphics.FromImage(bm)
    '        g.CopyFromScreen(Me.PointToScreen(p0), p0, bm.Size)
    '        'If dx > 0 Then
    '        '    g.CopyFromScreen(Me.PointToScreen(New Point(0, 0)), _
    '        '                             New Point(0, 0), bm.Size)
    '        'Else
    '        '    g.CopyFromScreen(Me.PointToScreen(New Point(dx, dy)), _
    '        '                              New Point(dx, dy), bm.Size)
    '        'End If


    '        ' Dim bm As New Bitmap(PictureBox1.Image, Me.PictureBox1.Size)
    '        Me.Cursor = New Cursor(bm.GetHicon)
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub
#End Region

    Private Sub TableLayoutPanel1_MouseHover(sender As Object, e As EventArgs) Handles TableLayoutPanel1.MouseHover, _
                tbZoom.MouseHover, btPanSud.MouseHover, btPanOvest.MouseHover, btPanNord.MouseHover, btPanNo.MouseHover, btPanEst.MouseHover, brZoomIndietro.MouseHover, brZoomAll.MouseHover
        Me.Opacity = 0.7

    End Sub

    Private Sub TableLayoutPanel1_MouseLeave(sender As Object, e As EventArgs) Handles TableLayoutPanel1.MouseLeave, _
                 tbZoom.MouseLeave, btPanSud.MouseLeave, btPanOvest.MouseLeave, btPanNord.MouseLeave, btPanNo.MouseLeave, btPanEst.MouseLeave, brZoomIndietro.MouseLeave, brZoomAll.MouseLeave
        ' Me.Opacity = 0.2
    End Sub


    Private Sub TableLayoutPanel1_MouseMove(sender As Object, e As MouseEventArgs) Handles TableLayoutPanel1.MouseMove

    End Sub

    Private Sub Panel1_MouseHover(sender As Object, e As EventArgs) Handles Panel1.MouseHover
        Me.Opacity = 0.2
    End Sub
End Class