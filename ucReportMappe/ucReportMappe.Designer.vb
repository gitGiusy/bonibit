﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucReportMappe
    Inherits System.Windows.Forms.UserControl

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucReportMappe))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tbZoom = New System.Windows.Forms.TrackBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.btZoom = New System.Windows.Forms.Button()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tsbZoomCompleto = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbStrade = New System.Windows.Forms.ToolStripButton()
        Me.tsbAerea = New System.Windows.Forms.ToolStripButton()
        Me.tsbSoloSelezionati = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbInformazioni = New System.Windows.Forms.ToolStripButton()
        Me.tsbZoomAvanti = New System.Windows.Forms.ToolStripButton()
        Me.tsbZoomIndietro = New System.Windows.Forms.ToolStripButton()
        Me.tsbZoomSelezione = New System.Windows.Forms.ToolStripButton()
        Me.tsbPan = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbA4 = New System.Windows.Forms.ToolStripButton()
        Me.tbVR = New System.Windows.Forms.TrackBar()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ContribuenteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PartitaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SiglaImpiantoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoImpiantoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZonaEcoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZonaIdrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SuperficieDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RenditaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NSubDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QualitàDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValidoDalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValidoAlDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IndiceEcoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IndiceIdrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IndiceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdUITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDGisDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdPartitaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdContribuenteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdSoggettoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SelezionatoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GeogDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bsUIT = New System.Windows.Forms.BindingSource(Me.components)
        Me.UITDataGridView = New System.Windows.Forms.DataGridView()
        CType(Me.tbZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.tbVR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsUIT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UITDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbZoom
        '
        Me.tbZoom.AutoSize = False
        Me.tbZoom.BackColor = System.Drawing.Color.White
        Me.tbZoom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbZoom.LargeChange = 1
        Me.tbZoom.Location = New System.Drawing.Point(0, 50)
        Me.tbZoom.Margin = New System.Windows.Forms.Padding(0)
        Me.tbZoom.Maximum = 5
        Me.tbZoom.Minimum = -5
        Me.tbZoom.Name = "tbZoom"
        Me.tbZoom.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbZoom.RightToLeftLayout = True
        Me.tbZoom.Size = New System.Drawing.Size(30, 145)
        Me.tbZoom.TabIndex = 15
        Me.tbZoom.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        Me.ToolTip1.SetToolTip(Me.tbZoom, "Zoom")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.Location = New System.Drawing.Point(0, 250)
        Me.Label2.Margin = New System.Windows.Forms.Padding(0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 20)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "D"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.Label2, "Definizione")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(0, 370)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 20)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "V"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ToolTip1.SetToolTip(Me.Label1, "Velocità")
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BackColor = System.Drawing.Color.White
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.White
        Me.SplitContainer1.Panel1.Controls.Add(Me.TableLayoutPanel2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.UITDataGridView)
        Me.SplitContainer1.Size = New System.Drawing.Size(848, 684)
        Me.SplitContainer1.SplitterDistance = 600
        Me.SplitContainer1.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.ReportViewer1, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(848, 600)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer1.DocumentMapWidth = 1
        Me.ReportViewer1.Location = New System.Drawing.Point(25, 0)
        Me.ReportViewer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(793, 600)
        Me.ReportViewer1.TabIndex = 3
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.btZoom, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.tbZoom, 0, 2)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(818, 0)
        Me.TableLayoutPanel4.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 8
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.03419!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.41661!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.41661!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.13259!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(30, 600)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'btZoom
        '
        Me.btZoom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btZoom.Font = New System.Drawing.Font("Calibri", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btZoom.Location = New System.Drawing.Point(0, 25)
        Me.btZoom.Margin = New System.Windows.Forms.Padding(0)
        Me.btZoom.Name = "btZoom"
        Me.btZoom.Size = New System.Drawing.Size(30, 25)
        Me.btZoom.TabIndex = 20
        Me.btZoom.Text = "0"
        Me.btZoom.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.ToolStrip1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.tbVR, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label1, 0, 3)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 6
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 250.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(25, 600)
        Me.TableLayoutPanel3.TabIndex = 1
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbZoomCompleto, Me.ToolStripSeparator1, Me.tsbStrade, Me.tsbAerea, Me.tsbSoloSelezionati, Me.ToolStripSeparator3, Me.tsbInformazioni, Me.tsbZoomAvanti, Me.tsbZoomIndietro, Me.tsbZoomSelezione, Me.tsbPan, Me.ToolStripSeparator2, Me.tsbA4})
        Me.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(25, 250)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tsbZoomCompleto
        '
        Me.tsbZoomCompleto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbZoomCompleto.Image = CType(resources.GetObject("tsbZoomCompleto.Image"), System.Drawing.Image)
        Me.tsbZoomCompleto.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbZoomCompleto.Name = "tsbZoomCompleto"
        Me.tsbZoomCompleto.Size = New System.Drawing.Size(23, 20)
        Me.tsbZoomCompleto.Text = "Zoom Completo"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(23, 6)
        '
        'tsbStrade
        '
        Me.tsbStrade.CheckOnClick = True
        Me.tsbStrade.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbStrade.Image = CType(resources.GetObject("tsbStrade.Image"), System.Drawing.Image)
        Me.tsbStrade.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbStrade.Name = "tsbStrade"
        Me.tsbStrade.Size = New System.Drawing.Size(23, 20)
        Me.tsbStrade.Text = "ToolStripButton2"
        Me.tsbStrade.ToolTipText = "Mosra Strade Bing"
        '
        'tsbAerea
        '
        Me.tsbAerea.CheckOnClick = True
        Me.tsbAerea.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAerea.Image = CType(resources.GetObject("tsbAerea.Image"), System.Drawing.Image)
        Me.tsbAerea.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbAerea.Name = "tsbAerea"
        Me.tsbAerea.Size = New System.Drawing.Size(23, 20)
        Me.tsbAerea.Text = "ToolStripButton1"
        Me.tsbAerea.ToolTipText = "Mostra Aerea Bing"
        '
        'tsbSoloSelezionati
        '
        Me.tsbSoloSelezionati.CheckOnClick = True
        Me.tsbSoloSelezionati.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSoloSelezionati.Image = CType(resources.GetObject("tsbSoloSelezionati.Image"), System.Drawing.Image)
        Me.tsbSoloSelezionati.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbSoloSelezionati.Name = "tsbSoloSelezionati"
        Me.tsbSoloSelezionati.Size = New System.Drawing.Size(23, 20)
        Me.tsbSoloSelezionati.Text = "ToolStripButton1"
        Me.tsbSoloSelezionati.ToolTipText = "Mostra solo gli elementi selezionati"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(23, 6)
        '
        'tsbInformazioni
        '
        Me.tsbInformazioni.Checked = True
        Me.tsbInformazioni.CheckOnClick = True
        Me.tsbInformazioni.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tsbInformazioni.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbInformazioni.Image = CType(resources.GetObject("tsbInformazioni.Image"), System.Drawing.Image)
        Me.tsbInformazioni.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbInformazioni.Name = "tsbInformazioni"
        Me.tsbInformazioni.Size = New System.Drawing.Size(23, 20)
        Me.tsbInformazioni.Text = "Informazioni"
        '
        'tsbZoomAvanti
        '
        Me.tsbZoomAvanti.CheckOnClick = True
        Me.tsbZoomAvanti.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbZoomAvanti.Image = CType(resources.GetObject("tsbZoomAvanti.Image"), System.Drawing.Image)
        Me.tsbZoomAvanti.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbZoomAvanti.Name = "tsbZoomAvanti"
        Me.tsbZoomAvanti.Size = New System.Drawing.Size(23, 20)
        Me.tsbZoomAvanti.Text = "Ingrandisci"
        '
        'tsbZoomIndietro
        '
        Me.tsbZoomIndietro.CheckOnClick = True
        Me.tsbZoomIndietro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbZoomIndietro.Image = CType(resources.GetObject("tsbZoomIndietro.Image"), System.Drawing.Image)
        Me.tsbZoomIndietro.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbZoomIndietro.Name = "tsbZoomIndietro"
        Me.tsbZoomIndietro.Size = New System.Drawing.Size(23, 20)
        Me.tsbZoomIndietro.Text = "Rimpicciolisci"
        '
        'tsbZoomSelezione
        '
        Me.tsbZoomSelezione.CheckOnClick = True
        Me.tsbZoomSelezione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbZoomSelezione.Image = CType(resources.GetObject("tsbZoomSelezione.Image"), System.Drawing.Image)
        Me.tsbZoomSelezione.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbZoomSelezione.Name = "tsbZoomSelezione"
        Me.tsbZoomSelezione.Size = New System.Drawing.Size(23, 20)
        Me.tsbZoomSelezione.Text = "Zoom alla Selezione"
        '
        'tsbPan
        '
        Me.tsbPan.CheckOnClick = True
        Me.tsbPan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPan.Image = CType(resources.GetObject("tsbPan.Image"), System.Drawing.Image)
        Me.tsbPan.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPan.Name = "tsbPan"
        Me.tsbPan.Size = New System.Drawing.Size(23, 20)
        Me.tsbPan.Text = "Sposta Mappa"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(23, 6)
        '
        'tsbA4
        '
        Me.tsbA4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsbA4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tsbA4.ForeColor = System.Drawing.Color.Navy
        Me.tsbA4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbA4.Name = "tsbA4"
        Me.tsbA4.Size = New System.Drawing.Size(23, 19)
        Me.tsbA4.Text = "A4"
        Me.tsbA4.ToolTipText = "Formato A4 orizzontale"
        '
        'tbVR
        '
        Me.tbVR.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.tbVR.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbVR.LargeChange = 1
        Me.tbVR.Location = New System.Drawing.Point(0, 270)
        Me.tbVR.Margin = New System.Windows.Forms.Padding(0)
        Me.tbVR.Maximum = 3
        Me.tbVR.Name = "tbVR"
        Me.tbVR.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.tbVR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbVR.Size = New System.Drawing.Size(25, 100)
        Me.tbVR.TabIndex = 14
        Me.tbVR.Value = 1
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ContribuenteDataGridViewTextBoxColumn, Me.PartitaDataGridViewTextBoxColumn, Me.SiglaImpiantoDataGridViewTextBoxColumn, Me.TipoImpiantoDataGridViewTextBoxColumn, Me.ZonaEcoDataGridViewTextBoxColumn, Me.ZonaIdrDataGridViewTextBoxColumn, Me.SuperficieDataGridViewTextBoxColumn, Me.RenditaDataGridViewTextBoxColumn, Me.NSubDataGridViewTextBoxColumn, Me.QualitàDataGridViewTextBoxColumn, Me.ValidoDalDataGridViewTextBoxColumn, Me.ValidoAlDataGridViewTextBoxColumn, Me.IndiceEcoDataGridViewTextBoxColumn, Me.IndiceIdrDataGridViewTextBoxColumn, Me.IndiceDataGridViewTextBoxColumn, Me.IdUITDataGridViewTextBoxColumn, Me.IDGisDataGridViewTextBoxColumn, Me.IdPartitaDataGridViewTextBoxColumn, Me.IdContribuenteDataGridViewTextBoxColumn, Me.IdSoggettoDataGridViewTextBoxColumn, Me.SelezionatoDataGridViewTextBoxColumn, Me.GeogDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.bsUIT
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(848, 80)
        Me.DataGridView1.TabIndex = 1
        '
        'ContribuenteDataGridViewTextBoxColumn
        '
        Me.ContribuenteDataGridViewTextBoxColumn.DataPropertyName = "Contribuente"
        Me.ContribuenteDataGridViewTextBoxColumn.HeaderText = "Contribuente"
        Me.ContribuenteDataGridViewTextBoxColumn.Name = "ContribuenteDataGridViewTextBoxColumn"
        Me.ContribuenteDataGridViewTextBoxColumn.ReadOnly = True
        Me.ContribuenteDataGridViewTextBoxColumn.Width = 150
        '
        'PartitaDataGridViewTextBoxColumn
        '
        Me.PartitaDataGridViewTextBoxColumn.DataPropertyName = "Partita"
        Me.PartitaDataGridViewTextBoxColumn.HeaderText = "Partita"
        Me.PartitaDataGridViewTextBoxColumn.Name = "PartitaDataGridViewTextBoxColumn"
        Me.PartitaDataGridViewTextBoxColumn.ReadOnly = True
        Me.PartitaDataGridViewTextBoxColumn.Width = 70
        '
        'SiglaImpiantoDataGridViewTextBoxColumn
        '
        Me.SiglaImpiantoDataGridViewTextBoxColumn.DataPropertyName = "SiglaImpianto"
        Me.SiglaImpiantoDataGridViewTextBoxColumn.HeaderText = "SiglaImpianto"
        Me.SiglaImpiantoDataGridViewTextBoxColumn.Name = "SiglaImpiantoDataGridViewTextBoxColumn"
        Me.SiglaImpiantoDataGridViewTextBoxColumn.ReadOnly = True
        Me.SiglaImpiantoDataGridViewTextBoxColumn.Width = 50
        '
        'TipoImpiantoDataGridViewTextBoxColumn
        '
        Me.TipoImpiantoDataGridViewTextBoxColumn.DataPropertyName = "TipoImpianto"
        Me.TipoImpiantoDataGridViewTextBoxColumn.HeaderText = "TipoImpianto"
        Me.TipoImpiantoDataGridViewTextBoxColumn.Name = "TipoImpiantoDataGridViewTextBoxColumn"
        Me.TipoImpiantoDataGridViewTextBoxColumn.ReadOnly = True
        Me.TipoImpiantoDataGridViewTextBoxColumn.Width = 70
        '
        'ZonaEcoDataGridViewTextBoxColumn
        '
        Me.ZonaEcoDataGridViewTextBoxColumn.DataPropertyName = "ZonaEco"
        Me.ZonaEcoDataGridViewTextBoxColumn.HeaderText = "ZonaEco"
        Me.ZonaEcoDataGridViewTextBoxColumn.Name = "ZonaEcoDataGridViewTextBoxColumn"
        Me.ZonaEcoDataGridViewTextBoxColumn.ReadOnly = True
        Me.ZonaEcoDataGridViewTextBoxColumn.Width = 50
        '
        'ZonaIdrDataGridViewTextBoxColumn
        '
        Me.ZonaIdrDataGridViewTextBoxColumn.DataPropertyName = "ZonaIdr"
        Me.ZonaIdrDataGridViewTextBoxColumn.HeaderText = "ZonaIdr"
        Me.ZonaIdrDataGridViewTextBoxColumn.Name = "ZonaIdrDataGridViewTextBoxColumn"
        Me.ZonaIdrDataGridViewTextBoxColumn.ReadOnly = True
        Me.ZonaIdrDataGridViewTextBoxColumn.Width = 50
        '
        'SuperficieDataGridViewTextBoxColumn
        '
        Me.SuperficieDataGridViewTextBoxColumn.DataPropertyName = "Superficie"
        Me.SuperficieDataGridViewTextBoxColumn.HeaderText = "Superficie"
        Me.SuperficieDataGridViewTextBoxColumn.Name = "SuperficieDataGridViewTextBoxColumn"
        Me.SuperficieDataGridViewTextBoxColumn.ReadOnly = True
        Me.SuperficieDataGridViewTextBoxColumn.Width = 50
        '
        'RenditaDataGridViewTextBoxColumn
        '
        Me.RenditaDataGridViewTextBoxColumn.DataPropertyName = "Rendita"
        Me.RenditaDataGridViewTextBoxColumn.HeaderText = "Rendita"
        Me.RenditaDataGridViewTextBoxColumn.Name = "RenditaDataGridViewTextBoxColumn"
        Me.RenditaDataGridViewTextBoxColumn.ReadOnly = True
        Me.RenditaDataGridViewTextBoxColumn.Width = 50
        '
        'NSubDataGridViewTextBoxColumn
        '
        Me.NSubDataGridViewTextBoxColumn.DataPropertyName = "nSub"
        Me.NSubDataGridViewTextBoxColumn.HeaderText = "nSub"
        Me.NSubDataGridViewTextBoxColumn.Name = "NSubDataGridViewTextBoxColumn"
        Me.NSubDataGridViewTextBoxColumn.ReadOnly = True
        Me.NSubDataGridViewTextBoxColumn.Width = 30
        '
        'QualitàDataGridViewTextBoxColumn
        '
        Me.QualitàDataGridViewTextBoxColumn.DataPropertyName = "Qualità"
        Me.QualitàDataGridViewTextBoxColumn.HeaderText = "Qualità"
        Me.QualitàDataGridViewTextBoxColumn.Name = "QualitàDataGridViewTextBoxColumn"
        Me.QualitàDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ValidoDalDataGridViewTextBoxColumn
        '
        Me.ValidoDalDataGridViewTextBoxColumn.DataPropertyName = "ValidoDal"
        Me.ValidoDalDataGridViewTextBoxColumn.HeaderText = "ValidoDal"
        Me.ValidoDalDataGridViewTextBoxColumn.Name = "ValidoDalDataGridViewTextBoxColumn"
        Me.ValidoDalDataGridViewTextBoxColumn.ReadOnly = True
        Me.ValidoDalDataGridViewTextBoxColumn.Width = 70
        '
        'ValidoAlDataGridViewTextBoxColumn
        '
        Me.ValidoAlDataGridViewTextBoxColumn.DataPropertyName = "ValidoAl"
        Me.ValidoAlDataGridViewTextBoxColumn.HeaderText = "ValidoAl"
        Me.ValidoAlDataGridViewTextBoxColumn.Name = "ValidoAlDataGridViewTextBoxColumn"
        Me.ValidoAlDataGridViewTextBoxColumn.ReadOnly = True
        Me.ValidoAlDataGridViewTextBoxColumn.Width = 70
        '
        'IndiceEcoDataGridViewTextBoxColumn
        '
        Me.IndiceEcoDataGridViewTextBoxColumn.DataPropertyName = "IndiceEco"
        Me.IndiceEcoDataGridViewTextBoxColumn.HeaderText = "IndiceEco"
        Me.IndiceEcoDataGridViewTextBoxColumn.Name = "IndiceEcoDataGridViewTextBoxColumn"
        Me.IndiceEcoDataGridViewTextBoxColumn.ReadOnly = True
        Me.IndiceEcoDataGridViewTextBoxColumn.Width = 30
        '
        'IndiceIdrDataGridViewTextBoxColumn
        '
        Me.IndiceIdrDataGridViewTextBoxColumn.DataPropertyName = "IndiceIdr"
        Me.IndiceIdrDataGridViewTextBoxColumn.HeaderText = "IndiceIdr"
        Me.IndiceIdrDataGridViewTextBoxColumn.Name = "IndiceIdrDataGridViewTextBoxColumn"
        Me.IndiceIdrDataGridViewTextBoxColumn.ReadOnly = True
        Me.IndiceIdrDataGridViewTextBoxColumn.Width = 30
        '
        'IndiceDataGridViewTextBoxColumn
        '
        Me.IndiceDataGridViewTextBoxColumn.DataPropertyName = "Indice"
        Me.IndiceDataGridViewTextBoxColumn.HeaderText = "Indice"
        Me.IndiceDataGridViewTextBoxColumn.Name = "IndiceDataGridViewTextBoxColumn"
        Me.IndiceDataGridViewTextBoxColumn.ReadOnly = True
        Me.IndiceDataGridViewTextBoxColumn.Width = 30
        '
        'IdUITDataGridViewTextBoxColumn
        '
        Me.IdUITDataGridViewTextBoxColumn.DataPropertyName = "idUIT"
        Me.IdUITDataGridViewTextBoxColumn.HeaderText = "idUIT"
        Me.IdUITDataGridViewTextBoxColumn.Name = "IdUITDataGridViewTextBoxColumn"
        Me.IdUITDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdUITDataGridViewTextBoxColumn.Width = 30
        '
        'IDGisDataGridViewTextBoxColumn
        '
        Me.IDGisDataGridViewTextBoxColumn.DataPropertyName = "IDGis"
        Me.IDGisDataGridViewTextBoxColumn.HeaderText = "IDGis"
        Me.IDGisDataGridViewTextBoxColumn.Name = "IDGisDataGridViewTextBoxColumn"
        Me.IDGisDataGridViewTextBoxColumn.ReadOnly = True
        Me.IDGisDataGridViewTextBoxColumn.Width = 30
        '
        'IdPartitaDataGridViewTextBoxColumn
        '
        Me.IdPartitaDataGridViewTextBoxColumn.DataPropertyName = "idPartita"
        Me.IdPartitaDataGridViewTextBoxColumn.HeaderText = "idPartita"
        Me.IdPartitaDataGridViewTextBoxColumn.Name = "IdPartitaDataGridViewTextBoxColumn"
        Me.IdPartitaDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdPartitaDataGridViewTextBoxColumn.Width = 30
        '
        'IdContribuenteDataGridViewTextBoxColumn
        '
        Me.IdContribuenteDataGridViewTextBoxColumn.DataPropertyName = "idContribuente"
        Me.IdContribuenteDataGridViewTextBoxColumn.HeaderText = "idContribuente"
        Me.IdContribuenteDataGridViewTextBoxColumn.Name = "IdContribuenteDataGridViewTextBoxColumn"
        Me.IdContribuenteDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdContribuenteDataGridViewTextBoxColumn.Width = 30
        '
        'IdSoggettoDataGridViewTextBoxColumn
        '
        Me.IdSoggettoDataGridViewTextBoxColumn.DataPropertyName = "idSoggetto"
        Me.IdSoggettoDataGridViewTextBoxColumn.HeaderText = "idSoggetto"
        Me.IdSoggettoDataGridViewTextBoxColumn.Name = "IdSoggettoDataGridViewTextBoxColumn"
        Me.IdSoggettoDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdSoggettoDataGridViewTextBoxColumn.Width = 30
        '
        'SelezionatoDataGridViewTextBoxColumn
        '
        Me.SelezionatoDataGridViewTextBoxColumn.DataPropertyName = "Selezionato"
        Me.SelezionatoDataGridViewTextBoxColumn.HeaderText = "Selezionato"
        Me.SelezionatoDataGridViewTextBoxColumn.Name = "SelezionatoDataGridViewTextBoxColumn"
        Me.SelezionatoDataGridViewTextBoxColumn.ReadOnly = True
        Me.SelezionatoDataGridViewTextBoxColumn.Width = 30
        '
        'GeogDataGridViewTextBoxColumn
        '
        Me.GeogDataGridViewTextBoxColumn.DataPropertyName = "geog"
        Me.GeogDataGridViewTextBoxColumn.HeaderText = "geog"
        Me.GeogDataGridViewTextBoxColumn.Name = "GeogDataGridViewTextBoxColumn"
        Me.GeogDataGridViewTextBoxColumn.ReadOnly = True
        Me.GeogDataGridViewTextBoxColumn.Visible = False
        '
        'bsUIT
        '
        Me.bsUIT.DataSource = GetType(ReportMappe.ucReportMappe.DataSetGis.sUIT)
        '
        'UITDataGridView
        '
        Me.UITDataGridView.AllowUserToAddRows = False
        Me.UITDataGridView.AllowUserToDeleteRows = False
        Me.UITDataGridView.AllowUserToOrderColumns = True
        Me.UITDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.UITDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UITDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.UITDataGridView.Name = "UITDataGridView"
        Me.UITDataGridView.ReadOnly = True
        Me.UITDataGridView.RowHeadersVisible = False
        Me.UITDataGridView.Size = New System.Drawing.Size(848, 80)
        Me.UITDataGridView.TabIndex = 0
        '
        'ucReportMappe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "ucReportMappe"
        Me.Size = New System.Drawing.Size(848, 684)
        CType(Me.tbZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.tbVR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsUIT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UITDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btZoom As System.Windows.Forms.Button
    Friend WithEvents tbZoom As System.Windows.Forms.TrackBar
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbStrade As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAerea As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbSoloSelezionati As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbVR As System.Windows.Forms.TrackBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents UITDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ContribuenteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PartitaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SiglaImpiantoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoImpiantoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ZonaEcoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ZonaIdrDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SuperficieDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RenditaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NSubDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QualitàDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValidoDalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValidoAlDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IndiceEcoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IndiceIdrDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IndiceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdUITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDGisDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdPartitaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdContribuenteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdSoggettoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SelezionatoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GeogDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bsUIT As System.Windows.Forms.BindingSource
    Friend WithEvents tsbZoomAvanti As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbZoomCompleto As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbZoomIndietro As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbInformazioni As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbZoomSelezione As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPan As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbA4 As System.Windows.Forms.ToolStripButton

End Class
